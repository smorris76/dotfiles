## If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

#export PATH=$HOME/local/bin:$PATH

function sshenv {
    export SSH_AUTH_SOCK=$1
}

#function dotfiles {
#   /usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME $@
#}
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

#
#function inbox {
#    if [[ $OS = "Darwin" ]]; then
#        gtimeout -s 9 900 offlineimap -1 -q -a $1 -f INBOX
#    else
#        timeout -s 9 900 offlineimap -1 -q -a $1 -f INBOX
#    fi
#}
#function nms {
#    while :
#    do
#        ssh -t nms1-dfw02.hwng.net screen -dR $HOSTNAME
#    done
#}
#function asn {   egrep "AS$1," ~/data/asns.csv; }
function asn {
    for as in "$@"
    do
        egrep "AS$as," ~/data/asns.csv
    done
}
function pluto {
    while :
    do
        ssh -t pluto screen -dR $HOSTNAME
    done
}
function slog {
    screen -t `basename $1` tail -f $1
}
function svim {
    screen -t `basename $1` vim $1
}
#
function tvi {
    tmux new-window -n vim:`basename $1` vim $1
}
#
function slogin {
    router=$1
    rtr=
    rtr=`echo $1 | sed 's/(.stackpath.systems|.hwng.net)//'`
    screen -t $rtr /usr/libexec/rancid/clogin $router
}
function tssh {
    tmux new-window -n "$1" ssh $1
}
function tl {
    if [[ $HOSTNAME =~ nms.*\.hwng\.net ]]; then
        tmux new-window -n `echo $1 | sed 's/\..*//'` $HOME/local/scripts/clogin.sh $1
    else
        tmux new-window -n `echo $1 | sed 's/\..*//'` clogin $1
    fi
}
#
#function do_delay {
#    ~/db/shawn/todo/todo.sh del $1 't:20[0-9][0-9]-[01][0-9]-[0-3][0-9]'
#    ~/db/shawn/todo/todo.sh app $1 $2
#}
#
#function delay {
#    if [[ -z "$1" ]]; then
#        echo "No date specified"
#        return
#    elif [[ $1 =~ (\+[0-9]+[wd]|(mon|tue|wed|thu|fri|sat|sun)) ]]; then
#        if [[ $OS = "Darwin" ]]; then
#            my_date="t:`date -v $1 +%Y-%m-%d`"
#        else
#            my_date="t:`date -d $1 +%Y-%m-%d`"
#        fi
#    elif [[ $1 =~ '[0-9]{4}\-[0-9]{2}\-[0-9]{2}' ]]; then
#        my_date="t:$1"
#    else
#        echo "Invalid date specified"
#        return
#    fi
#    shift
#    if [[ -z "$1" ]]; then
#        echo "No task specified"
#        return
#    fi
#	while [[ $1 ]]
#	do
#        if [[ $1 =~ ^[0-9]+$ ]]; then
#            do_delay $1 $my_date
#        elif [[ $1 =~ ^[0-9]+\-[0-9]+$ ]]; then
#            my_start=`echo $1 | sed -E 's/-[0-9]+//'`
#            my_end=`echo $1 | sed -E 's/[0-9]+\-//'`
#            while [[ $my_start -le $my_end ]]
#            do
#                do_delay $my_start $my_date
#                ((my_start++))
#            done
#        fi
#        shift
#	done
#}
#
function myos {
    echo $OS
}
#
export OS=`uname`
## https://superuser.com/questions/1195962/cannot-make-directory-var-run-screen-permission-denied#1195963
if [[ `uname -a | grep Linux.*Microsoft` ]]; then
    export SCREENDIR=$HOME/.screen_tmp
fi
#
6p() { curl -s -F "content=<${1--}" -F ttl=604800 -w "%{redirect_url}\n" -o /dev/null https://p.6core.net/; }
bind "set completion-ignore-case on"
##- & _ are equivalent for completion. requires completion-ignore-case
bind "set completion-map-case on"
##only 1 tab to see possible completions
bind "set show-all-if-ambiguous on"
#
## append to the history file, don't overwrite it
shopt -s histappend
## the below two are not working on 10.11.3
## shopt -s autocd
##shopt -s dirspell
shopt -s cdspell
#
##HISTFILESIZE=1000000
##HISTSIZE=1000000
#
HISTCONTROL=ignoreboth
##HISTIGNORE='ls:bg:fg:history'
HISTTIMEFORMAT='%F %T '
#HISTTIMEFORMAT="%m/%d %T "
##PROMPT_COMMAND='history -a'
#
if [[ $OS = "Linux" ]]; then
    alias open='/mnt/c/Windows/System32/cmd.exe /c start'
fi
alias 80cols='curl https://web00.smorris.net/~shawn/80cols.txt'
alias 132cols='curl https://web00.smorris.net/~shawn/132cols.txt'
#alias tmosh='mosh --ssh="ssh -p 1976" home.smorris.com'
#alias nmutt='mutt -F ~/dotfiles/muttrc.buster.offlineimap'
#alias snmutt='screen -t NTT mutt -F ~/dotfiles/muttrc.buster.offlineimap'
#alias gmutt='mutt -F ~/dotfiles/muttrc.gmail.offlineimap'
#alias sgmutt='screen -t GMAIL mutt -F ~/dotfiles/muttrc.gmail.offlineimap'
#alias gmb='screen -t gmb ~/db/shawn/Scripts/getmail.sh buster'
#alias gmg='screen -t gmg ~/db/shawn/Scripts/getmail.sh gmail'
#alias gmgl='screen -t gmg-lite ~/db/shawn/Scripts/getmail-lite.sh gmail'
#alias gmn='screen -t gmn ~/db/shawn/Scripts/getmail.sh ntt'
#alias gmnl='screen -t gmn-lite ~/db/shawn/Scripts/getmail-lite.sh ntt INBOX'
alias ll="ls -lAh"
alias lm="ls -la | less"
if [[ $HOSTNAME =~ nms.*\.hwng\.net ]]; then
    export PATH=/opt/nms/bin:$PATH
    export LD_LIBRARY_PATH=/opt/nms/lib:$LD_LIBRARY_PATH
    export MANPATH=/opt/nms/share/man:$MANPATH
    alias jlogin=/usr/libexec/rancid/jlogin
    alias clogin=/usr/libexec/rancid/clogin
fi
#alias mac2unix="tr '\r' '\n'"
#alias mvim='~/db/shawn/bin/mvim'
#alias nofflineimap='rm ~/.offlineimap/ntt.lock ; offlineimap ; kill -9 `launchctl list | grep offlineimap | cut -f 1`'
#alias projects='cat /Users/shawn/db/shawn/docs/notes/projects.txt'
#alias unix2mac="tr '\n' '\r'"
alias rsync="rsync --progress -rputve ssh"
#alias scratch="mvim ~/db/shawn/docs/notes/scratch-$HOSTNAME.txt"
#alias dtd='td -d ~/db/shawn/todo/todo_del.cfg'
#alias ddtd='td -d ~/db/shawn/todo/todo_del.cfg del'
#alias ddtl='td -d ~/db/shawn/todo/todo_del.cfg ls'
#alias ddtm='td -d ~/db/shawn/todo/todo_del.cfg mv'
#alias eng-drop='rsync smorris@eng0.gin.ntt.net:eng-drop ~'
alias refresh='source ~/.bashrc'
#alias td="~/db/shawn/todo/todo.sh"
#alias tda="~/db/shawn/todo/todo.sh -t add"
#alias tdc="~/db/shawn/todo/todo.sh do"
#alias tdd="~/db/shawn/todo/todo.sh del"
#alias tdl="~/db/shawn/todo/todo.sh ls"
#alias tdll="~/db/shawn/todo/todo.sh ls | less"
#alias tdln="clear;~/db/shawn/todo/todo.sh ls @na"
#alias tdlno="clear;~/db/shawn/todo/todo.sh ls @na -@online"
#alias tdlnw='~/db/shawn/todo/todo.sh ls @na +w'
#alias tdm="~/db/shawn/todo/todo.sh mv"
#alias tdp="~/db/shawn/todo/todo.sh app"
#alias tdw="~/db/shawn/todo/todo.sh ls @na +w"
#alias tdvi='vim ~/db/shawn/todo/todo.txt'
#alias projects='td lsprj'
#alias deferred='cat ~/db/shawn/todo/deferred.txt'
#alias xterm="xterm -fg white -bg black"
#alias vmrun="/Applications/VMware\ Fusion.app/Contents/Library/vmrun"
alias mouse="killall LogiMgrDaemon"
alias nms2="ssh -t nms1-dfw02.hwng.net screen -x $HOSTNAME"
alias nms="ssh -t nms1-dfw02.hwng.net screen -dR remote"
alias stats="ssh -t stats1.hwng.net screen -dR remote"
alias tmpvi='tmux new-window -n vim:tmp.txt vim $HOME/tmp/tmp.txt'

unset color_prompt

#
#PS1='\e[36;1m\]\u\e[32m@\]\h:\e[34m\w\e[37\$\e[0m\$ '
case "$TERM" in
    screen|xterm-color|*-256color) color_prompt=yes;;
esac


if [ "$color_prompt" = yes ]; then
    PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='\u@\h:\w\$ '
fi

#
if [ `hostname -s` == "MBA-10003456" ];
then
    alias pongovnc="open vnc://pongo.local"
fi

if [ -d "/opt/homebrew" ];
then
    export HOMEBREW_PREFIX="/opt/homebrew";
    export HOMEBREW_CELLAR="/opt/homebrew/Cellar";
    export HOMEBREW_REPOSITORY="/opt/homebrew";
    export PATH="/opt/homebrew/bin:/opt/homebrew/sbin${PATH+:$PATH}";
    export MANPATH="/opt/homebrew/share/man${MANPATH+:$MANPATH}:";
    export INFOPATH="/opt/homebrew/share/info:${INFOPATH:-}";
fi
if [ $OS == "Darwin" ];
then
    alias code='/Applications/Visual\ Studio\ Code.app/Contents/Resources/app/bin/code'
    alias tmpcp='cat $HOME/tmp/tmp.txt | pbcopy'
    #PATH=$PATH:/usr/local/sbin
fi
export PATH PS1
#export TODOTXT_FINAL_FILTER=~/db/shawn/todo/futureTasks
if [[ `uname -a | grep Linux.*Microsoft` ]]; then
    cd
fi
