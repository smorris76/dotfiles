source ~/.bashrc
# the following allows for quick pasting to p.6core.net, usuage is "p filename"
# or "echo hi | p" the ssh back to the host will enter the url into your
# clipboard, thus # saving you some manual copy&paste with the trackpad. Just
# use command-v to # paste it somewhere

function p {
    curl -s -F "content=<${1--}" -F ttl=604800 -w "%{redirect_url}\n" \
			    -o /dev/null https://p.6core.net/ | \
				    tr -d '\n' | tee /dev/stderr | pbcopy
	    echo
}
