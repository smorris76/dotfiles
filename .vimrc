filetype on
"filetype plugin indent on
syntax on

"enable pathogen for managing bundles
"execute pathogen#infect()

"color
set termguicolors
colorscheme catppuccin_mocha
set background=dark

"incremental search
set incsearch

set ignorecase
set smartcase

"show line numbers
"set number

"autoindent
set ai

"expand tabs
set expandtab

"tab stops=4 spaces
set ts=4

" when using the >> or << commands, shift lines by 4 spaces
set shiftwidth=4

" show a visual line under the cursor's current line
"set cursorline

" show the matching part of the pair for [] {} and ()
set showmatch

"give 2 lines of context
set scrolloff=2

set foldmethod=indent
set foldlevel=99

"au BufRead /var/tmp/mutt-* set fo+=w tw=72
"au BufRead /var/tmp/mutt* set fo+=w tw=72
"au BufRead /private/var/*/mutt* set fo+=w tw=72 nonumber
au BufRead mutt-* set fo+=w tw=72 nonumber

au BufRead *.mmd set tw=72

"augroup filetypedetect
"  " Mail
"  autocmd BufRead,BufNewFile *mutt-*              setfiletype mail
"augroup END
"augroup filetypedetct
"    autocmd BufRead,BufNewFile *mutt-*  set nonumber
"augroup END

:nnoremap <leader>m :silent !open -a Marked.app '%:p'<cr>
